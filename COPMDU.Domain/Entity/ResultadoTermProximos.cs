﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Domain.Entity
{
    /// <summary>
    /// Classe que retorna a lista dos terminais próximos do contrato
    /// </summary>
    public class ResultadoTermProximos
    {
        private readonly List<Item> _items = new List<Item>();

        /// <summary>
        /// Classe com os itens da consulta
        /// </summary>
        public class Item
        {
            /// <summary>
            /// Contrato
            /// </summary>
            public int Contrato { get; set; }

            /// <summary>
            /// MAC Adress
            /// </summary>
            public string MAC { get; set; }

            /// <summary>
            /// Tipo do terminal
            /// </summary>
            public string Terminal { get; set; }

            /// <summary>
            /// Endereco do terminal
            /// </summary>
            public string Endereco { get; set; }
        }


        /// <summary>
        /// Retorna se a sessão está ok
        /// </summary>
        public bool SessaoOK
        {
            get;
            set;
        }

        /// <summary>
        /// Coleção de itens
        /// </summary>
        public IList<Item> Items
        {
            get
            {
                return _items;

            }
        }
    }
}
