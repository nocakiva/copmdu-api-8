﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain
{
    public class Login
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Version { get; set; }
    }
}
